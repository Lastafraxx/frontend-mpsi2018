<?php
  include "conn.php";
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- CSS Saya -->
    <link rel="stylesheet" href="style.css">

    <!-- Font Viga -->
    <link href="https://fonts.googleapis.com/css?family=Viga" rel="stylesheet">

    <!-- peta unand -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKH2F9gZMQyATwBodQsEr-uM0fokVCvZw&callback=initMap"></script>

    <script>

    var marker;
      function initialize() {



        // Variabel untuk menyimpan informasi (desc)
        var infoWindow = new google.maps.InfoWindow;

        //  Variabel untuk menyimpan peta Roadmap
        var mapOptions = {
           zoom: 4,
           mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        // Pembuatan petanya
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        // Variabel untuk menyimpan batas kordinat
        var bounds = new google.maps.LatLngBounds();

        // Pengambilan data dari database
        <?php
            $query = mysqli_query($conn,"select * from tbl_lokasi");
            if(mysqli_num_rows($query) < 1){?>
               //peta tanpa marker-2.5446949,118.3207873,5.29z
        var properti_peta = {
                    center: new google.maps.LatLng(-2.5446949, 118.3207873),
                    zoom: 4,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                 var peta = new google.maps.Map(document.getElementById("map"), properti_peta);
             //end


<?php
            }else{
            while ($data = mysqli_fetch_array($query))
            {
                $nama = mystripslashesjs($data['namalokasi']);
                $alamat = mystripslashesjs($data['alamat']);
                $lat = $data['lat'];
                $lng = $data['lng'];
                $alamat = str_replace(array("\r","\n"),"",$alamat);
                echo ("addMarker($lat, $lng, '<b>$nama</b><br>$alamat');");

            }
            }
          ?>

        // Proses membuat marker
        function addMarker(lat, lng, info) {
            var lokasi = new google.maps.LatLng(lat, lng);
            bounds.extend(lokasi);
            var marker = new google.maps.Marker({
                map: map,
                 position: lokasi,

            });
            map.fitBounds(bounds);
            bindInfoWindow(marker, map, infoWindow, info);
         }

        // Menampilkan informasi pada masing-masing marker yang diklik
        function bindInfoWindow(marker, map, infoWindow, html) {
          google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
          });
        }

        }
      google.maps.event.addDomListener(window, 'load', initialize);



     $(document).ready(function(){
        $('#modal-edit').on('show.bs.modal', function (e) {
            var idx = $(e.relatedTarget).data('id');
             $.ajax({
                type : 'post',
                url : 'detaildata.php',
                data :  'idx='+ idx,
                success : function(data){
                $('.hasil-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });


      </script>

    <title>Peta Unand</title>
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
      <a class="navbar-brand" href="index.php">PETAUNAND</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="#">Features</a>
            <a class="nav-item nav-link" href="#">About</a>
            <a class="nav-item nav-link btn btn-primary tombol" href="#">Login</a>
          </div>
      </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">Aplikasi <span>Peta Unand</span> Terpadu</h1>
          <a href="" class="btn btn-primary tombol">Lebih Lanjut</a>
      </div>
    </div>
    <!-- Akhir Jumbotron -->

    <!-- Container -->
    <div class="container">
      <!-- Info Panel -->
      <div class="row justify-content-center">
        <div class="col-10 info-panel">
          <div class="row">
            <div class="col-lg">
              <img src="img/kaca-pembesar.png" alt="" class="float-left">
              <h4>Pencarian</h4>
              <p>Pencarian data lengkap untuk fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="img/security.png" alt="" class="float-left">
              <h4>Informasi</h4>
              <p>Informasi lengkap terkait dengan penggunaan fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="img/map-localization.png" alt="" class="float-left">
              <h4>Navigasi</h4>
              <p>Petunjuk untuk menuju ke titik fasilitas yang ada di Universitas Andalas</p>
            </div>
          </div>
        </div>
      </div>
      <!-- Akhir info panel -->

      <!-- Konten -->
      <div class="row workingspace">
        <div class="col-lg-6 img-fluid" id="map" style="height:400px">
          <!-- <img src="img/workingspace.png" alt="" class="img-fluid"> -->
        </div>
        <div class="col-lg-5">
          <h3><span>Cari</span> Tempat <span>Tujuan Mu</span></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint commodi aliquid similique rem tenetur laboriosam dicta provident, quasi id quibusdam.</p>
          <a href="detailinfo.php" class="btn btn-primary tombol">Lanjut</a>
        </div>
      </div>
      <div class="row workingspace justify-content-center">
        <h3 class="display-4">Data Fasilitas</h3>
      </div>
      <div class="row workingspace">
        <table class="table table-hover table-bordered">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama fasilitas</th>
              <th scope="col">Keterangan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Larry</td>
              <td> the Bird</td>
              <td>@twitter</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- Akhir Konten -->

      <!-- Testimoni -->
      <section class="testimonial">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h5>Aplikasi ini diharapkan dapat membantu mahasiswa atau pengunjung dalam menemukan fasilitas yang ada di kampus Universitas Andalas ini</h5>
          </div>
        </div>

        <div class="row justify-content-center">
          <div class="col-6 justify-content-center d-flex">
            <figure class="figure">
              <img src="img/hafiz.jpg" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
            <figure class="figure">
              <img src="img/hafiz.jpg" class="figure-img img-fluid rounded-circle utama" alt="testi1">
                <figcaption class="figure-caption">
                  <h5>Hafiz</h5>
                  <p>Mahasiswa</p>
                </figcaption>
            </figure>
            <figure class="figure">
              <img src="img/hafiz.jpg" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
          </div>
        </div>
      </section>
      <!-- Akhir Testimoni -->
    </div>
    <!-- Akhir Container -->

    <!-- Footer -->
    <div class="row">
      <div class="col text-center footer">
        <p>2018 &copy All Rights Reserved by Sistem Informasi Universitas Andalas</p>
      </div>
    </div>
    <!-- Akhir Footer -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
